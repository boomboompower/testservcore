/*
   Copyright (C) 2016 boomboompower

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package me.boomboompower.testserv.commands;

/*
* Made for TestServ Core
* by boomboompower 27/04/2016
*/

import me.boomboompower.testserv.TestServCore;

public class TestServCoreCommands {

    private TestServCore testServCore;

    public TestServCoreCommands(TestServCore testServCore) {
        this.testServCore = testServCore;

        new TestServCoreExperimental(testServCore);
        new TestServCoreCreateWorld(testServCore);
        new TestServCoreReload(testServCore);
        new TestServCoreRemove(testServCore);
        new TestServCoreSpawn(testServCore);
        new TestServCoreAlert(testServCore);
        new TestServCoreItems(testServCore);
        new TestServCoreStop(testServCore);
        new TestServCoreWarp(testServCore);
        new TestServCoreSudo(testServCore);
        new TestServCoreKick(testServCore);
        new TestServCoreHelp(testServCore);
        new TestServCoreBan(testServCore);
        new TestServCoreFly(testServCore);
        new TestServCoreOp(testServCore);
    }
}
