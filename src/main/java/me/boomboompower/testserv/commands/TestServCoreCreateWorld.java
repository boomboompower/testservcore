/*
   Copyright (C) 2016 boomboompower

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package me.boomboompower.testserv.commands;

/*
* Made for TestServCore
* by boomboompower 11/06/2016
*/

import me.boomboompower.testserv.TestServCore;
import me.boomboompower.testserv.generators.TestServCoreDefaultGenerator;
import me.boomboompower.testserv.utils.Register;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.boomboompower.testserv.utils.Utils.*;

public class TestServCoreCreateWorld implements CommandExecutor {

    private TestServCore testServCore;

    private String command = "aCreateWorld";

    public TestServCoreCreateWorld(TestServCore testServCore) {
        this.testServCore = testServCore;

        Register.registerCommands(command, this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (cmd.getName().equalsIgnoreCase(command)) {
            if (sender instanceof Player && !permissionCheck((Player) sender, "testServ.createWorld")) {
                sendUnknownMessage((Player) sender);
            } else {
                if (args.length < 1) {
                    duelSend(sender, "&cIncorrect usage. Use &4/createworld <worldname>&c instead!");
                } else {
                    boolean bool = false;
                    for (World world : Bukkit.getWorlds()) {
                        if (args[0].equalsIgnoreCase(world.getName())) {
                            bool = true;
                        }
                    }
                    if (!bool) {
                        WorldCreator wc = new WorldCreator("Server_" + args[0]);
                        wc.type(WorldType.FLAT);
                        wc.generator(new TestServCoreDefaultGenerator());
                        wc.generateStructures(true);
                        World world = wc.createWorld();
                        world.setGameRuleValue("mobGriefing", "false");
                        world.setGameRuleValue("doMobSpawning", "false");
                        world.setGameRuleValue("creatorName", sender.getName());
                        if (sender instanceof Player) world.setGameRuleValue("creatorUUID", ((Player) sender).getUniqueId().toString());
                    } else {
                        duelSend(sender, "&cA world with that name already exists!");
                    }

                }
            }
        }
        return true;
    }
}
