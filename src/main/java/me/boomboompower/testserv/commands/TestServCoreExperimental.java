/*
   Copyright (C) 2016 boomboompower

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package me.boomboompower.testserv.commands;

/*
* Made for TestServ Core
* by boomboompower 17/06/2016
*/

import me.boomboompower.testserv.TestServCore;
import me.boomboompower.testserv.noreflection.NMSUtils;
import me.boomboompower.testserv.utils.Register;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.boomboompower.testserv.utils.Utils.*;

public class TestServCoreExperimental implements CommandExecutor {

    private TestServCore testServCore;

    private String command = "aExperimental";

    public TestServCoreExperimental(TestServCore testServCore) {
        this.testServCore = testServCore;

        Register.registerCommands(command, this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (cmd.getName().equalsIgnoreCase(command)) {
            if (sender instanceof Player) {
                Player p = (Player) sender;
                if (!permissionCheck(p, "testServ.experimental")) {
                    sendDenyMessage(p);
                } else {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        NMSUtils.clearChat(player);
                        NMSUtils.clearActionBar(player);
                    }
                }
            } else {
                sendToConsole("&cOnly a player can use this :P");
            }
        }
        return true;
    }
}