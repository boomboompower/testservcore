package me.boomboompower.testserv.commands;

/*
* Made for TestServ Core
* by boomboompower 25/04/2016
*/

import me.boomboompower.testserv.TestServCore;
import me.boomboompower.testserv.utils.ItemUtils;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;

import static me.boomboompower.testserv.utils.Register.*;
import static me.boomboompower.testserv.utils.Utils.*;

public class TestServCoreItems implements CommandExecutor {

    private TestServCore testServCore;

    private String command = "item";

    public TestServCoreItems(TestServCore testServCore) {
        this.testServCore = testServCore;

        registerCommands(command, this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (cmd.getName().equalsIgnoreCase(command)) {
            if (sender instanceof Player) {
                Player p = (Player) sender;
                if (!permissionCheck(p, "testServ.item")) {
                    sendDenyMessage(p);
                } else {
                    if (args.length < 1) {
                        sendToPlayer(p, "&cInvalid arguments. Use &4/item <item> &c!");
                    } else {
                        if (itemTest("pickaxe", args)) {
                            ItemUtils.addItem(p, ItemUtils.getSuperPickaxe());
                        } else if (itemTest("sword", args)) {
                            ItemUtils.addItem(p, ItemUtils.getSlayerSword());
                        } else if (itemTest("dragon", args)) {
                            if (permissionCheck(p, "testServ.item.special")) {
                                ItemUtils.addItem(p, ItemUtils.getDragonEgg());
                            } else {
                                sendDenyMessage(p);
                            }
                        } else {
                            sendToPlayer(p, "&4The item specified did not exist :P");
                            sendToPlayer(p, "&4Your arguments: &c" + args[0] + "&4.");
                        }
                    }
                }
            } else {
                sendToConsole("&cOnly a player can use this command... Sorry!");
            }
        }
        return true;
    }

    private boolean itemTest(String item, String[] args) {
        return args[0].equalsIgnoreCase(item);
    }
}