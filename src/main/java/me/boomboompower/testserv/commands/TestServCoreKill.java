/*  
   Copyright (C) 2016 boomboompower

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package me.boomboompower.testserv.commands;
/*
* Made for TestServCore
* by boomboompower 17/07/2016
*/

import me.boomboompower.testserv.TestServCore;
import me.boomboompower.testserv.utils.Register;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.boomboompower.testserv.utils.Utils.*;

public class TestServCoreKill implements CommandExecutor {

    private TestServCore testServCore;
    private String command = "aKill";

    public TestServCoreKill(TestServCore testServCore) {
        this.testServCore = testServCore;

        Register.registerCommands(command, testServCore);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (cmd.getName().equalsIgnoreCase(command)) {
            if (sender instanceof Player) {
                Player p = (Player) sender;

                sendUnknownMessage(p);
            } else {
                if (args.length < 1) {
                    sendToConsole("&cInvalid arguments. Use &4/kill <player> &c!");
                } else if (args[0] != null) {
                    boolean done = false;
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        if (args[0].equals(player.getName()) && player.isOnline() && !player.isDead() && player.getHealth() > 0.0D) {
                            player.setHealth(0.0D);
                            sendToPlayer(player, "&4&lDEATH: &cYou were killed by &6&lCONSOLE&c.");
                            sendToConsole("&aSuccessfully killed &2" + player.getName() + "&a!");
                            done = true;
                            break;
                        }
                    } if (!done) {
                        sendToConsole("&cCould not find &4" + args[0] + "&c. Make sure they are online & alive!");
                    }
                }
            }
        }
        return true;
    }
}
