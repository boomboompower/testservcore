package me.boomboompower.testserv.commands;

/*
* Made for TestServ Core
* by boomboompower 25/04/2016
*/

import me.boomboompower.testserv.TestServCore;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Date;

import static me.boomboompower.testserv.utils.Register.registerCommands;
import static me.boomboompower.testserv.utils.Utils.*;

public class TestServCoreReload implements CommandExecutor {

    private TestServCore testServCore;

    private String command = "aReload";

    public TestServCoreReload(TestServCore testServCore) {
        this.testServCore = testServCore;

        registerCommands(command, this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (cmd.getName().equalsIgnoreCase(command)) {
            if (sender instanceof Player) {
                Player p = (Player) sender;

                sendUnknownMessage(p);
            } else {
                boolean fin = false;
                int date = new Date().getSeconds();
                sendToConsole("&cReloading the server...");
                for (int time = 0; !fin; time++) {
                    Bukkit.reload();
                    Bukkit.reloadWhitelist();
                }
                date = new Date().getSeconds() - date;
                sendToConsole("&aDone. Took: &2" + date + "&a seconds!");
                fin = true;
            }
        }
        return true;
    }
}