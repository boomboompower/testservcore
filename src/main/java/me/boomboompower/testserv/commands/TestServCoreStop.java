package me.boomboompower.testserv.commands;

/*
* Made for TestServ Core
* by boomboompower 25/04/2016
*/

import me.boomboompower.testserv.TestServCore;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.boomboompower.testserv.utils.Register.registerCommands;
import static me.boomboompower.testserv.utils.Utils.*;

public class TestServCoreStop implements CommandExecutor {

    private TestServCore testServCore;

    private String command = "aStop";

    public TestServCoreStop(TestServCore testServCore) {
        this.testServCore = testServCore;

        registerCommands(command, this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (cmd.getName().equalsIgnoreCase(command)) {
            if (sender instanceof Player) {
                Player p = (Player) sender;

                sendUnknownMessage(p);
            } else {
                sendToConsole("&cStopping the server...");
                Bukkit.shutdown();
                sendToConsole("&cSuccessfully stopped.");
                sendToConsole("&ePress any key to close this window.");
            }
        }
        return true;
    }
}