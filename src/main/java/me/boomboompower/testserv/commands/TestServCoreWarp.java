package me.boomboompower.testserv.commands;

/*
* Made for TestServ Core
* by boomboompower 25/04/2016
*/

import me.boomboompower.testserv.TestServCore;

import me.boomboompower.testserv.exceptions.CommandException;
import me.boomboompower.testserv.enums.Worlds;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.boomboompower.testserv.utils.Register.registerCommands;
import static me.boomboompower.testserv.utils.Utils.*;

public class TestServCoreWarp implements CommandExecutor {

    private TestServCore testServCore;

    private String command = "aWarp";

    public TestServCoreWarp(TestServCore testServCore) {
        this.testServCore = testServCore;

        registerCommands(command, this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (cmd.getName().equalsIgnoreCase(command)) {
            if (sender instanceof Player) {
                Player p = (Player) sender;
                if (!permissionCheck(p, "testServ.Warp")) {
                    sendDenyMessage(p);
                } else {
                    if (args.length < 1) {
						sendToPlayer(p, "Invalid arguments. Use &4/warp <world>");
					} else {
                        try {
                            if (argsTest("overworld", args)) {
                                sendToWorld(p, Worlds.OVERWORLD.getWorld());
                            } else if (argsTest("nether", args)) {
                                sendToWorld(p, Worlds.NETHER.getWorld());
                            } else if (argsTest("end", args)) {
                                sendToWorld(p, Worlds.END.getWorld());
                            } else if (argsTest("loot", args)) {
                                sendToWorld(p, Worlds.LOOT_TABLES.getWorld());
                            } else {
                                try {
                                    sendToWorld(p, Bukkit.getWorld("Server_" + args[0]));
                                } catch (Exception ex) {
                                    sendToPlayer(p, "&cSorry the world: &4" + args[0] + "&c does not exist!");
                                }
                            }
                        } catch (Exception ex) {
                            throw new CommandException("An error occured whilst trying to send " + p.getName() + " to " + args[0]);
                        }
					}
                }
            } else {
                if (args.length < 1) {
                    sendToConsole("&cTry using &4/warp <world> <player>");
                } else if (args.length > 1) {
                    Player p = Bukkit.getPlayer(args[1]);
                    boolean bool = false;
                    if (p != null && p.isOnline()) {
                        try {
                            if (argsTest("overworld", args)) {
                                sendToWorld(p, Worlds.OVERWORLD.getWorld());
                                bool = true;
                            } else if (argsTest("nether", args)) {
                                sendToWorld(p, Worlds.NETHER.getWorld());
                                bool = true;
                            } else if (argsTest("end", args)) {
                                sendToWorld(p, Worlds.END.getWorld());
                                bool = true;
                            } else if (argsTest("loot", args)) {
                                sendToWorld(p, Worlds.LOOT_TABLES.getWorld());
                                bool = true;
                            } else {
                                sendToConsole("&cSorry the world: &4" + args[1] + "&c does not exist!");
                            }
                            if (bool) sendToConsole("&cSent &4" + args[0] + "&c to &4" + args[1] + "&c!");
                        } catch (Exception ex) {
                            throw new CommandException("An error occured whilst trying to send " + args[0] + " to " + args[1]);
                        }
                    } else {
                        if (p != null) sendToConsole("&cSorry, &4" + p.getName() + "&c is not online!");
                    }
                }
            }
        }
        return true;
    }

    private boolean argsTest(String worldName, String[] args) {
        return args[0].equalsIgnoreCase(worldName);
    }
}