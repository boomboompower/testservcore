package me.boomboompower.testserv.enums;

/*
* Made for TestServCore
* by boomboompower 12/06/2016
*/

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public enum Players {

    BOOMBOOMPOWER(Bukkit.getPlayer("boomboompower"), Rank.OWNER),
    COSMOCRAFTER2(Bukkit.getPlayer("cosmocrafter2"), Rank.NORMAL);

    private String displayTag;
    private Player player;
    private String name;
    private String tag;
    private Rank rank;

    Players(Player player, Rank rank) {
        this.displayTag = rank.getDisplayTag();
        this.player = player;
        this.name = player.getName();
        this.tag = rank.getTag();
        this.rank = rank;
    }

    public String getDisplayTag() {
        return getRank().getDisplayTag();
    }

    public Player getPlayer() {
        return this.player;
    }

    public String getTag() {
        return getRank().getTag();
    }

    public String getName() {
        return this.name;
    }

    public Rank getRank() {
        return this.rank;
    }
}
