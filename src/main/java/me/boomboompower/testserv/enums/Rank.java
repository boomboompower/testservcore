package me.boomboompower.testserv.enums;

/*
* Made for TestServCore
* by boomboompower 12/06/2016
*/

public enum Rank {

    OWNER("Owner", "OWNER"),
    ADMIN("Administrator", "ADMIN"),
    BUILDER("Builder", "BUILDER"),
    DEV("Developer", "DEV"),
    MOD("Moderator", "MOD"),
    DONOR("Donator", "DONOR"),
    NORMAL("Player", "");

    private String tag;
    private String display;

    Rank(String tag) {
        this.tag = tag;
    }

    Rank(String tag, String display) {
        this.tag = tag;
        this.display = display;
    }

    public String getTag() {
        return this.tag;
    }

    public String getDisplayTag() {
        return this.display != null ? this.display : "None";
    }
}
