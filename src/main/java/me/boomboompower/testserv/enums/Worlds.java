package me.boomboompower.testserv.enums;

/*
* Made for TestServCore
* by boomboompower 10/06/2016
*/

import me.boomboompower.testserv.utils.Utils;

import org.bukkit.World;

public enum Worlds {

    // Base worlds
    OVERWORLD(Utils.getWorldFromString("Server")),
    NETHER(Utils.getWorldFromString("Server_nether")),
    END(Utils.getWorldFromString("Server_the_end")),

    LOOT_TABLES(Utils.getWorldFromString("Server_loottables"));

    public World world;
    public String worldName;

    Worlds(World world) {
        this.world = world;
        this.worldName = world.getName();
    }

    public World getWorld() {
        return world;
    }

    public String getWorldName() {
        return worldName;
    }
}
