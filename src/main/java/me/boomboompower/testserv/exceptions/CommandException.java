package me.boomboompower.testserv.exceptions;

/*
* Made for TestServCore
* by boomboompower 07/06/2016
*/

public class CommandException extends RuntimeException {

    private static final long serialVersionUID = 1239880L;

    public CommandException() {
        super("An error occured while using a command.");
    }

    public CommandException(String message) {
        super(message);
    }
}
