package me.boomboompower.testserv.exceptions;

/*
* Made for TestServCore
* by boomboompower 07/06/2016
*/

public class EventException extends RuntimeException {

    private static final long serialVersionUID = 1239890L;

    public EventException() {
        super("An error occured from an event.");
    }

    public EventException(String message) {
        super(message);
    }
}
