package me.boomboompower.testserv.exceptions;

/*
* Made for TestServCore
* by boomboompower 11/06/2016
*/

public class FlyCommandException extends CommandException {

    private static final long serialVersionUID = 1239881L;

    public FlyCommandException() {
        super("A command generated an unexpected exception.");
    }

    public FlyCommandException(String message) {
        super(message);
    }
}
