package me.boomboompower.testserv.exceptions;

/*
* Made for TestServCore
* by boomboompower 07/06/2016
*/

public class GhostPlayerPacketException extends PacketException {

    private static final long serialVersionUID = 1239872L;

    public GhostPlayerPacketException() {
        super("The ghost player at spawn generated an unexpected exception.");
    }

    public GhostPlayerPacketException(String message) {
        super(message);
    }
}
