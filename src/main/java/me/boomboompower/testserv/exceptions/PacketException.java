package me.boomboompower.testserv.exceptions;

/*
* Made for TestServCore
* by boomboompower 07/06/2016
*/

public class PacketException extends RuntimeException {

    private static final long serialVersionUID = 1239870L;

    public PacketException() {
        super("An error occured while sending a packet.");
    }

    public PacketException(String message) {
        super(message);
    }

    public PacketException(int type) {
        super("An error occured while sending a packet. With a type ID of: " + type);
    }
}
