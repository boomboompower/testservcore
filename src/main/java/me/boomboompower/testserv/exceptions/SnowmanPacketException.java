package me.boomboompower.testserv.exceptions;

/*
* Made for TestServCore
* by boomboompower 07/06/2016
*/

public class SnowmanPacketException extends PacketException {

    private static final long serialVersionUID = 1239871L;

    public SnowmanPacketException() {
        super("A snowman generated an unexpected exception.");
    }

    public SnowmanPacketException(String message) {
        super(message);
    }
}
