package me.boomboompower.testserv.generators;

/*
* Made for TestServCore
* by boomboompower 11/06/2016
*/

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.ChunkGenerator;

import java.util.Random;

public class TestServCoreDefaultGenerator extends ChunkGenerator {

    public byte[][] generateBlockSections(World world, Random random, int chunkX, int chunkZ, BiomeGrid biomeGrid) {
        byte[][] result = new byte[world.getMaxHeight() / 16][]; //world height / chunk part height (=16, look above)

        for(int x = 0; x < 16; x++) {
            for(int z = 0; z < 16; z++) {
                setBlock(result, x, 0, z, (byte) Material.BEDROCK.getId());
            }
        }

        for(int x = 0; x < 16; x++) {
            for(int y = 1; y <= 32; y++) {
                for (int z = 0; z < 16; z++) {
                    setBlock(result, x, y, z, (byte) Material.STAINED_CLAY.getId());
                }
            }
        }

        for(int x = 0; x < 16; x++) {
            for(int z = 0; z < 16; z++) {
                setBlock(result, x, 33, z, (byte) Material.HARD_CLAY.getId());
            }
        }

        return result;
    }

    void setBlock(byte[][] result, int x, int y, int z, byte byteId) {
        // is this chunk part already initialized?
        if (result[y >> 4] == null) {
            // Initialize the chunk part
            result[y >> 4] = new byte[4096];
        }
        // set the block (look above, how this is done)
        result[y >> 4][((y & 0xF) << 8) | (z << 4) | x] = byteId;
    }
}
