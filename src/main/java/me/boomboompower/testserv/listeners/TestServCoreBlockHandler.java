package me.boomboompower.testserv.listeners;

/*
* Made for TestServ Core
* by boomboompower 26/04/2016
*/

import me.boomboompower.testserv.TestServCore;
import me.boomboompower.testserv.utils.ItemUtils;
import me.boomboompower.testserv.utils.Register;
import me.boomboompower.testserv.utils.Utils;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;

import java.util.ArrayList;

import static me.boomboompower.testserv.utils.Utils.*;

public class TestServCoreBlockHandler implements Listener {

    private TestServCore testServCore;

    private ArrayList<String> listBreakOne;
    private ArrayList<String> listBreakTwo;

    private ArrayList<String> listPlaceOne;
    private ArrayList<String> listPlaceTwo;

    public TestServCoreBlockHandler(TestServCore testServCore) {
        this.testServCore = testServCore;

        this.listBreakOne = new ArrayList<String>();
        this.listPlaceOne = new ArrayList<String>();
        this.listBreakTwo = new ArrayList<String>();
        this.listPlaceTwo = new ArrayList<String>();

        Register.registerEvents(this);
    }

    @EventHandler(priority = EventPriority.HIGH)
    private void onEntityChangeBlock(EntityChangeBlockEvent e) {
        if (e.getEntity() instanceof Enderman) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    private void onBlockBreak(BlockBreakEvent e) {
        Player p = e.getPlayer();
        if (p.getGameMode() == GameMode.CREATIVE) return;
        if (ItemUtils.isSuperBreaker(ItemUtils.getItemInMainHand(p))) {
            if (Utils.permissionCheck(p, "testServ.SuperBreaker")) {
                Block block = e.getBlock();
                breakBlocks(block, 1);
            } else {
                denyBreak(p);
            }
        } else if (!permissionCheck(p, "testServ.Break")) {
            e.setCancelled(true);
            denyBreak(p);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    private void onBlockPlace(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        if (p.getGameMode() == GameMode.CREATIVE) return;
        if (ItemUtils.isDragonEgg(ItemUtils.getItemInMainHand(p))) {
          e.setCancelled(true);
        } else if (!permissionCheck(p, "testServ.Break")) {
            e.setCancelled(true);
            denyPlace(p);
        }
    }

    private void denyBreak(Player p) {
        if (listBreakOne.contains(p.getName())) {
            sendToPlayer(p, denyMessage("break blocks", "Final warning"));
            listBreakOne.remove(p.getName());
            listBreakTwo.add(p.getName());
        } else if (listBreakTwo.contains(p.getName())) {
            p.kickPlayer(translate(kickMessage()));
            listBreakTwo.remove(p.getName());
        } else {
            sendToPlayer(p, denyMessage("break blocks", "First warning"));
            listBreakOne.add(p.getName());
        }
    }

    private void denyPlace(Player p) {
        if (listPlaceOne.contains(p.getName())) {
            sendToPlayer(p, denyMessage("place blocks", "Final warning"));
            listPlaceOne.remove(p.getName());
            listPlaceTwo.add(p.getName());
        } else if (listPlaceTwo.contains(p.getName())) {
            p.kickPlayer(kickMessage());
            listPlaceTwo.remove(p.getName());
        } else {
            sendToPlayer(p, denyMessage("place blocks", "First warning"));
            listPlaceOne.add(p.getName());
        }
    }

    private void breakBlocks(Block start, int radius) {
        if (radius < 0) return;
        for (int x = -radius; x <= radius; x++) {
            for (int y = -radius; y <= radius; y++) {
                for (int z = -radius; z <= radius; z++) {
                    Block b = start.getRelative(x, y, z);
					if (b.getType() != Material.BEDROCK && b.getType() != Material.BARRIER) {
                        b.breakNaturally();
					}
                }
            }
        }
    }
}