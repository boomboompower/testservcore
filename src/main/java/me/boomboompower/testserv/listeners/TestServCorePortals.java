package me.boomboompower.testserv.listeners;

/*
* Made for TestServ Core
* by boomboompower 28/04/2016
*/

import me.boomboompower.testserv.TestServCore;
import me.boomboompower.testserv.utils.Register;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEnterEvent;
import org.bukkit.event.entity.EntityPortalExitEvent;
import org.bukkit.event.world.PortalCreateEvent;

import java.util.ArrayList;

import static me.boomboompower.testserv.utils.Utils.*;

public class TestServCorePortals implements Listener {

    private TestServCore testServCore;
    private ArrayList<Player> cool = new ArrayList<Player>();

    public TestServCorePortals(TestServCore testServCore) {
        this.testServCore = testServCore;

        Register.registerEvents(this);
    }

    @EventHandler
    private void onEntityPortalEnter(EntityPortalEnterEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            if (cool.contains(player)) return;
            cool.add(player);
            sendToPlayer(player, "&fYou are filled with the essence of the portal...");
        }
    }

    @EventHandler
    private void onEntityPortalExit(EntityPortalExitEvent event) {
        Location l = event.getEntity().getLocation();
        event.setAfter(l.getDirection().normalize().multiply(3));
        event.getEntity().setGlowing(false);

        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            cool.remove(player);
            sendToPlayer(player, "&fThe essence slowly fades away...");
        }
    }

    @EventHandler
    private void onPortalCreate(PortalCreateEvent e) {
        e.setCancelled(true);
        for (Block block : e.getBlocks()) {
            block.breakNaturally();
        }
    }
}