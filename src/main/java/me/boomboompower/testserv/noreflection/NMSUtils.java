package me.boomboompower.testserv.noreflection;

/*
* Made for TestServCore
* by boomboompower 17/06/2016
*/

import net.minecraft.server.v1_10_R1.*;

import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

@Deprecated // Contains unsafe methods.
public class NMSUtils {

    @Deprecated
    public static void clearHeaderFooter(Player player) {
        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();
        sendPacket(packet, getCraftPlayer(player));
    }

    @Deprecated
    public static void clearChat(Player player) {
        PacketPlayOutChat packet = new PacketPlayOutChat(new ChatComponentText(""));
        int max = 5000;
        for (int i = 0; i < max; i++) {
            sendPacket(packet, getCraftPlayer(player));
        }
    }

    @Deprecated
    public static void die(Player player) {
        EntityPlayer p = getCraftPlayer(player);
        p.die(DamageSource.OUT_OF_WORLD);
    }

    @Deprecated
    public static void clearActionBar(Player player) {
        PacketPlayOutChat packet = new PacketPlayOutChat(new ChatComponentText(""), (byte) 2);
        sendPacket(packet, getCraftPlayer(player));
    }

    private static void sendPacket(Packet<?> packet, EntityPlayer player) {
        player.playerConnection.sendPacket(packet);
    }

    private static EntityPlayer getCraftPlayer(Player player) {
        return ((CraftPlayer) player).getHandle();
    }
}
