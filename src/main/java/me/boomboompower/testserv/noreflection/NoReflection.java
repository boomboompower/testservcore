package me.boomboompower.testserv.noreflection;

/*
* Made for TestServCore
* by boomboompower 13/05/2016
*/

import me.boomboompower.testserv.TestServCore;

public class NoReflection {

    private TestServCore testServCore;

    public NoReflection(TestServCore testServCore) {
        this.testServCore = testServCore;

        new TestServCoreGhost(testServCore);
        new TestServCorePlayer(testServCore);
        new TestServCoreSnowman(testServCore);
    }
}
