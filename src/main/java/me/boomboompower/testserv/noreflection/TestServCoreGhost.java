package me.boomboompower.testserv.noreflection;

/*
* Made for TestServCore
* by boomboompower 08/06/2016
*/

import com.mojang.authlib.GameProfile;

import me.boomboompower.testserv.TestServCore;
import me.boomboompower.testserv.exceptions.GhostPlayerPacketException;
import me.boomboompower.testserv.utils.Register;
import me.boomboompower.testserv.utils.Utils;

import net.minecraft.server.v1_10_R1.*;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_10_R1.CraftServer;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.UUID;

public class TestServCoreGhost implements Listener {

    private UUID ghostUUID = UUID.randomUUID();
    private String ghostName = "Notch";

    private HashMap<Player, Integer> interact = new HashMap<Player, Integer>();

    private TestServCore testServCore;
    private EntityPlayer npc;

    public TestServCoreGhost(TestServCore testServCore) {
        this.testServCore = testServCore;
        this.npc = new EntityPlayer(((CraftServer) Bukkit.getServer()).getServer(), getNMSWorld(), new GameProfile(ghostUUID, ghostName), new PlayerInteractManager(getNMSWorld()));

        Register.registerEvents(this);
    }

    @EventHandler
    private void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        ghostNotNull(npc);
        npc.setInvisible(true);
        npc.teleportTo(Utils.spawnLocation(getNMSWorld().getWorld()), false);

        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, npc));
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutNamedEntitySpawn(npc));
    }

    @EventHandler
    private void onGhostInteract(PlayerInteractEntityEvent event) {
        Player p = event.getPlayer();
        if (interact.containsKey(p)) {
            if (interact.get(p) > 50) {
                Utils.sendToPlayer(p, "&7[&c" + ghostName + "&7] &fYou appear to be hacking.");
                waitForKick(p, "&7[&c" + ghostName + "&7] &4Did you know that hacking is not allowed.", 2);
            }
        }
        if (event.getRightClicked().getEntityId() == npc.getBukkitEntity().getEntityId()) {
            Utils.sendToPlayer(p, "&7[&c" + ghostName + "&7] &fHello " + p.getName() + "...");
            interact.put(p, interact.get(p));
        }
    }

    @EventHandler
    private void onPlayerMove(PlayerMoveEvent event) {
        npc.teleportTo(event.getTo(), false);
        PacketPlayOutEntityTeleport packet = new PacketPlayOutEntityTeleport(npc);

        EntityPlayer entityPlayer = getCraftPlayer(event.getPlayer());
        entityPlayer.playerConnection.sendPacket(packet);
    }

    private WorldServer getNMSWorld() {
        if (testServCore.isEnabled()) {
            return ((CraftWorld) Bukkit.getWorlds().get(0)).getHandle();
        } else {
            return getNMSWorld();
        }
    }

    private void ghostNotNull(Object... a) {
        for (Object b : a) {
            if (b == null) throw new GhostPlayerPacketException();
        }
    }

    private void waitForKick(final Player p, final String message, int time) {
        new BukkitRunnable() {
            @Override
            public void run() {
                p.kickPlayer(Utils.translate(message));
            }
        }.runTaskLater(testServCore, time * 20);
    }

    private EntityPlayer getCraftPlayer(Player player) {
        return ((CraftPlayer) player).getHandle();
    }
}
