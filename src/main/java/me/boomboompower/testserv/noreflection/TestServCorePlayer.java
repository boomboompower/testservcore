package me.boomboompower.testserv.noreflection;

/*
* Made for TestServCore
* by boomboompower 08/06/2016
*/

import me.boomboompower.testserv.TestServCore;
import me.boomboompower.testserv.utils.Register;

import net.minecraft.server.v1_10_R1.*;

import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.HashMap;
import java.util.Map;

public class TestServCorePlayer implements Listener {

    private TestServCore testServCore;

    private NBTTagCompound nbt = new NBTTagCompound();
    private Map<EntityPlayer, Integer> cooldown = new HashMap<EntityPlayer, Integer>(); // TODO prevent combat logging

    public TestServCorePlayer(TestServCore testServCore) {
        this.testServCore = testServCore;

        nbt.setShort("Damage", (short) 50);

        Register.registerEvents(this);
    }

    @EventHandler
    private void onItem(PlayerItemHeldEvent event) {
        ItemStack item = getCraftPlayer(event.getPlayer()).getItemInMainHand();
        if (item != null && item.getTag() != null && item.getTag().getString("heldBy") != null) item.getTag().setString("heldBy", event.getPlayer().getName());
    }

    @EventHandler
    private void onPlayerDeath(PlayerDeathEvent event) {
        EntityPlayer p = getCraftPlayer(event.getEntity());
        PacketPlayInClientCommand packet = new PacketPlayInClientCommand(PacketPlayInClientCommand.EnumClientCommand.PERFORM_RESPAWN);
        p.playerConnection.a(packet);
    }

    @EventHandler
    private void onPlayerJoin(PlayerJoinEvent event) {
        final EntityPlayer p = getCraftPlayer(event.getPlayer());
    }

    @EventHandler
    private void onPlayerInteract(PlayerInteractEvent event) {
        Block b = event.getClickedBlock();
        EntityPlayer p = getCraftPlayer(event.getPlayer());
        if (b != null) {
            if (b.getType() == Material.SIGN || b.getType() == Material.WALL_SIGN || b.getType() == Material.SIGN_POST) {
                p.attack(p);
                try {
                    if (p.getEnderChest() != null) {
                        ItemStack[] s = p.getEnderChest().getContents().clone();
                        for (int i = 0; i < s.length; i++) {
                            if (s[i].getItem() != null) {
                                s[i].addEnchantment(Enchantment.c(0), 5);
                                s[i].addEnchantment(Enchantment.c(16), 5);
                            }
                        }
                    }
                } catch (Exception ex) {}
            }
        }
    }

    private void sendPacket(EntityPlayer player, Packet<?> packet) {
        player.playerConnection.sendPacket(packet);
    }

    private EntityPlayer getCraftPlayer(Player player) {
        return ((CraftPlayer) player).getHandle();
    }
}
