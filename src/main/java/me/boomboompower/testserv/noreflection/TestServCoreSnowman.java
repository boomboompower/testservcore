package me.boomboompower.testserv.noreflection;

/*
* Made for TestServCore
* by boomboompower 08/06/2016
*/

import me.boomboompower.testserv.TestServCore;
import me.boomboompower.testserv.noreflection.customentities.EntitySnowman;
import me.boomboompower.testserv.exceptions.SnowmanPacketException;
import me.boomboompower.testserv.utils.Register;

import me.boomboompower.testserv.utils.Utils;

import net.minecraft.server.v1_10_R1.*;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class TestServCoreSnowman implements Listener {

    private TestServCore testServCore;
    private EntitySnowman snowman;

    public TestServCoreSnowman(TestServCore testServCore) {
        this.testServCore = testServCore;
        this.snowman = new EntitySnowman(getNMSWorld());

        Register.registerEvents(this);
    }

    @EventHandler
    private void onPlayerJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();

        snowmanNotNull(snowman);
        registerSnowmanAbilities(snowman);
        getNMSWorld().getWorld().spawn(Utils.spawnLocation(getNMSWorld().getWorld()), snowman.getBukkitEntity().getClass());
    }


    private void snowmanNotNull(Object... a) {
        for (Object b : a) {
            if (b == null) throw new SnowmanPacketException();
        }
    }

    private void registerSnowmanAbilities(EntitySnowman snowman) {
        snowmanNotNull(snowman);

        snowman.l(true);
        snowman.setSize(1.7F, 2.9F);
        snowman.forceExplosionKnockback = false;
    }

    private void moveSnowman(EntitySnowman a, int b, Location c) {
        snowmanNotNull(a, b, c);

        double a1 = c.getX();
        double a2 = c.getY();
        double a3 = c.getZ();

        switch (b) {
            case 0:
                a.teleportTo(c, false);
                break;
            case 1:
                a.enderTeleportTo(a1, a2, a3);
                break;
            case 2:
                a.move(a1, a2, a3);
                break;
            case 3:
                a.setLocation(a1, a2, a3, 0F, 0F);
                break;
            default:
                a.setPosition(a1, a2, a3);
                break;
        }
    }

    private WorldServer getNMSWorld() {
        return ((CraftWorld) Bukkit.getWorlds().get(0)).getHandle();
    }
}
