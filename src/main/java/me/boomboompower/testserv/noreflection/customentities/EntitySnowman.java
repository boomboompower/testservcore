package me.boomboompower.testserv.noreflection.customentities;

/*
* Made for TestServCore
* by boomboompower 12/06/2016
*/

import net.minecraft.server.v1_10_R1.*;
import org.bukkit.craftbukkit.v1_10_R1.event.CraftEventFactory;
import org.bukkit.craftbukkit.v1_10_R1.util.CraftMagicNumbers;
import org.bukkit.event.block.EntityBlockFormEvent;

public class EntitySnowman extends EntityGolem implements IRangedEntity {

    private static final DataWatcherObject<Byte> a;

    static {
        a = DataWatcher.a(EntitySnowman.class, DataWatcherRegistry.a);
    }

    public EntitySnowman(World world) {
        super(world);
        this.setSize(0.7F, 1.9F);
    }

    public static void b(DataConverterManager dataconvertermanager) {
        EntityInsentient.a(dataconvertermanager, "SnowMan");
    }

    protected void r() {
        this.goalSelector.a(1, new PathfinderGoalArrowAttack(this, 1.25D, 50, 20.0F));
        this.goalSelector.a(2, new PathfinderGoalRandomStroll(this, 1.0D));
        this.goalSelector.a(3, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 6.0F));
        this.goalSelector.a(4, new PathfinderGoalRandomLookaround(this));
        this.targetSelector.a(1, new PathfinderGoalNearestAttackableTarget(this, EntityInsentient.class, 10, true, false, IMonster.d));
    }

    protected void initAttributes() {
        super.initAttributes();
        this.getAttributeInstance(GenericAttributes.maxHealth).setValue(20.0D);
        this.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(0.30000000298023224D);
    }

    protected void i() {
        super.i();
        this.datawatcher.register(a, (byte) 0);
    }

    public void n() {
        super.n();
        if(!this.world.isClientSide) {
            int i = MathHelper.floor(this.locX);
            int j = MathHelper.floor(this.locY);
            int k = MathHelper.floor(this.locZ);
            if(this.ai()) {
                this.damageEntity(DamageSource.DROWN, 1.0F);
            }

            if(this.world.getBiome(new BlockPosition(i, 0, k)).a(new BlockPosition(i, j, k)) > 1.0F) {
                this.damageEntity(CraftEventFactory.MELTING, 1.0F);
            }

            if(!this.world.getGameRules().getBoolean("mobGriefing")) {
                return;
            }

            for(int l = 0; l < 4; ++l) {
                i = MathHelper.floor(this.locX + (double)((float)(l % 2 * 2 - 1) * 0.25F));
                j = MathHelper.floor(this.locY);
                k = MathHelper.floor(this.locZ + (double)((float)(l / 2 % 2 * 2 - 1) * 0.25F));
                BlockPosition blockposition = new BlockPosition(i, j, k);
                if(this.world.getType(blockposition).getMaterial() == Material.AIR && this.world.getBiome(new BlockPosition(i, 0, k)).a(blockposition) < 0.8F && Blocks.SNOW_LAYER.canPlace(this.world, blockposition)) {
                    org.bukkit.block.BlockState blockState = this.world.getWorld().getBlockAt(i, j, k).getState();
                    blockState.setType(CraftMagicNumbers.getMaterial(Blocks.SNOW_LAYER));
                    EntityBlockFormEvent event = new EntityBlockFormEvent(this.getBukkitEntity(), blockState.getBlock(), blockState);
                    this.world.getServer().getPluginManager().callEvent(event);
                    if(!event.isCancelled()) {
                        blockState.update(true);
                    }
                }
            }
        }

    }

    protected MinecraftKey J() {
        return LootTables.A;
    }

    public void a(EntityLiving entityliving, float f) {
        EntitySnowball entitysnowball = new EntitySnowball(this.world, this);
        double d0 = entityliving.locY + (double)entityliving.getHeadHeight() - 1.100000023841858D;
        double d1 = entityliving.locX - this.locX;
        double d2 = d0 - entitysnowball.locY;
        double d3 = entityliving.locZ - this.locZ;
        float f1 = MathHelper.sqrt(d1 * d1 + d3 * d3) * 0.2F;
        entitysnowball.shoot(d1, d2 + (double)f1, d3, 1.6F, 12.0F);
        this.a(SoundEffects.fW, 1.0F, 1.0F / (this.getRandom().nextFloat() * 0.4F + 0.8F));
        this.world.addEntity(entitysnowball);
    }

    public float getHeadHeight() {
        return 1.7F;
    }

    protected boolean a(EntityHuman entityhuman, EnumHand enumhand, ItemStack itemstack) {
        if(itemstack != null && itemstack.getItem() == Items.SHEARS && !this.isDerp() && !this.world.isClientSide) {
            this.setDerp(true);
            itemstack.damage(1, entityhuman);
        }

        return super.a(entityhuman, enumhand, itemstack);
    }

    public boolean isDerp() {
        return (this.datawatcher.get(a) & 16) != 0;
    }

    public void setDerp(boolean flag) {
        byte b0 = this.datawatcher.get(a);
        if(flag) {
            this.datawatcher.set(a, (byte) (b0 | 16));
        } else {
            this.datawatcher.set(a, (byte) (b0 & -17));
        }

    }

    protected SoundEffect G() {
        return SoundEffects.fT;
    }

    protected SoundEffect bV() {
        return SoundEffects.fV;
    }

    protected SoundEffect bW() {
        return SoundEffects.fU;
    }
}
