package me.boomboompower.testserv.utils;

/*
* Made for TestServCore
* by boomboompower 10/06/2016
*/

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

import static me.boomboompower.testserv.utils.Utils.*;

public class ItemUtils {

    public static ItemStack getDragonEgg() {
        ItemStack egg = new ItemStack(Material.DRAGON_EGG);
        ItemMeta meta = egg.getItemMeta();

        meta.setDisplayName(translate("&9&lDragon Egg"));

        egg.setItemMeta(meta);
        return egg;
    }

    public static ItemStack getSuperPickaxe() {
        ItemStack pick = new ItemStack(Material.GOLD_PICKAXE);
        ItemMeta meta = pick.getItemMeta();

        setLore(meta, "&eBreak multiple blocks at once!");
        meta.setDisplayName(translate("&6&lSuper Breaker"));
        meta.spigot().setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_UNBREAKABLE);

        pick.addUnsafeEnchantment(Enchantment.DIG_SPEED, 10);
        pick.addUnsafeEnchantment(Enchantment.MENDING, 5);
        pick.setItemMeta(meta);

        return pick;
    }

    public static ItemStack getSlayerSword() {
        ItemStack sword = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta meta = sword.getItemMeta();

        meta.setDisplayName(translate("&9&lSword of the slayer"));

        sword.addUnsafeEnchantment(Enchantment.DURABILITY, 5);
        sword.setItemMeta(meta);

        return sword;
    }

    public static ItemStack getUltraShield() {
        ItemStack item = new ItemStack(Material.SHIELD, 1);
        ItemMeta meta = item.getItemMeta();

        meta.spigot().setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meta.setDisplayName(translate("&9&lUltra Shield"));

        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack getWorldSelector() {
        ItemStack worldItem = new ItemStack(Material.WATCH, 1);
        ItemMeta worldMeta = worldItem.getItemMeta();

        worldMeta.setDisplayName(translate("&aWorld Selector"));
        worldItem.setItemMeta(worldMeta);

        return worldItem;
    }

    public static boolean isDragonEgg(ItemStack item) {
        return item.equals(getDragonEgg());
    }

    public static boolean isSuperBreaker(ItemStack item) {
        return item.equals(getSuperPickaxe());
    }

    public static boolean isSlayerSword(ItemStack item) {
        return item.equals(getSlayerSword());
    }

    public static boolean isUltraShield(ItemStack item) {
        return item.equals(getUltraShield());
    }

    public static boolean isWorldSelector(ItemStack item) {
        return item.equals(getWorldSelector());
    }

    public static void setLore(ItemMeta meta, String... lore) {
        List<String> l = new ArrayList<String>();
        for (String s : lore) {
            l.add(translate(s));
        }
        meta.setLore(l);
    }

    public static void addItem(Player player, ItemStack item) {
        player.getInventory().addItem(item);
    }

    public static ItemStack getItemInMainHand(Player player) {
        return player.getInventory().getItemInMainHand();
    }

    public static ItemStack getItemInOffHand(Player player) {
        return player.getInventory().getItemInOffHand();
    }
}
