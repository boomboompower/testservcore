package me.boomboompower.testserv.utils;

/*
* Made for TestServ Core
* by boomboompower 30/04/2016
*/

import me.boomboompower.testserv.exceptions.PacketException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

@SuppressWarnings("ALL") // Don't want warnings in this class.
public class ReflectionUtils {

    public static void sendHeaderFooter(Player a, String b, String c) {
        try {
            b = Utils.translate(b);
            c = Utils.translate(c);

            Object d = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + b + "\"}");
            Object e = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + c + "\"}");
            Object f = getNMSClass("PacketPlayOutPlayerListHeaderFooter").getConstructor(getNMSClass("IChatBaseComponent")).newInstance(d);

            setValue("a", f, d);
            setValue("b", f, e);

            Object g = a.getClass().getMethod("getHandle").invoke(a);
            Object h = g.getClass().getField("playerConnection").get(g);

            h.getClass().getMethod("sendPacket", getNMSClass("Packet")).invoke(h, f);
        } catch (Exception ex) {
            throw new PacketException(1);
        }
    }

    public static void sendPlayOutChat(Player a, String b, int c) {
        try {
            b = Utils.translate(b);

            Object d = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + b + "\"}");
            Object e = getNMSClass("PacketPlayOutChat").getConstructor(getNMSClass("IChatBaseComponent"), byte.class).newInstance(d, (byte) c);

            Object f = a.getClass().getMethod("getHandle").invoke(a);
            Object g = f.getClass().getField("playerConnection").get(f);

            g.getClass().getMethod("sendPacket", getNMSClass("Packet")).invoke(g, e);
        } catch (Exception e) {
            throw new PacketException(2);
        }
    }

    public static ItemStack setUnbreakable(ItemStack a, int b) {
        try {
            Class<?> c = getCraftBukkitClass("inventory.CraftItemStack");
            Method d = c.getMethod("asNMSCopy", ItemStack.class);
            Object e = d.invoke(null, a);
            Object f = getNMSClass("NBTTagCompound").getConstructor().newInstance();
            f.getClass().getMethod("setInt", String.class, int.class).invoke(f, "Unbreakable", b);
            e.getClass().getMethod("setTag", f.getClass()).invoke(e, f);
            a = (ItemStack) c.getMethod("asCraftMirror", e.getClass()).invoke(null, e);

            return a;
        } catch (Exception e) {
            throw new PacketException(3);
        }
    }

    public static Field getValue(String fieldName, Object instance) {
        try {
            Field field = instance.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            return field;
        } catch (Exception ex) {
            throw new PacketException(4);
        }
    }

    public static void setValue(String fieldName, Object instance, Object value) {
        try {
            Field field = instance.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(instance, value);
        } catch (Exception ex) {
            throw new PacketException(5);
        }
    }

    public static Class<?> getCraftBukkitClass(String name) {
        try {
            return Class.forName("org.bukkit.craftbukkit." + getVersion() + "." + name);
        } catch (Exception ex) {
            throw new PacketException(5);
        }
    }

    public static Class<?> getNMSClass(String name) {
        try {
            return Class.forName("net.minecraft.server." + getVersion() + "." + name);
        } catch (Exception ex) {
            throw new PacketException(6);
        }
    }

    public static String getVersion() {
        return Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
    }

    public enum Types {

        CHAT(0),
        SYSTEM(1),
        ACTION_BAR(2);

        private int type;

        Types(int type) {
            this.type = type;
        }

        public int getValue() {
            return type;
        }
    }
}
