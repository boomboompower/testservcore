package me.boomboompower.testserv.utils;

/*
* Made for TestServ Core
* by boomboompower 28/04/2016
*/

import me.boomboompower.testserv.TestServCore;
import me.boomboompower.testserv.exceptions.CommandException;
import me.boomboompower.testserv.exceptions.EventException;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.event.Listener;

public class Register {

    // If your wondering why we didn't just
    // Put this in the Utils class, it's
    // Because we wanted the Utils class to
    // Be usable from anywhere without needing
    // The restrictions for only TestServCore

    private static TestServCore testServCore;

    public Register(TestServCore testServCore) {
        this.testServCore = testServCore;
    }

    public static void registerEvents(Listener listener) {
        try {
            Bukkit.getPluginManager().registerEvents(listener, testServCore);
        } catch (Exception ex) {
            throw new EventException("While registering the " + listener.getClass().getSimpleName() + " listener. An error occured!");
        }
    }

    public static void registerCommands(String command, CommandExecutor commandExecutor) {
        try {
            testServCore.getCommand(command).setExecutor(commandExecutor);
        } catch (Exception ex) {
            throw new CommandException("While setting the " + command + " command an unexpected error occured!");
        }
    }
}
